extends StaticBody2D

var player = null
var parent = null
var size = Vector2(4095, 0)
var next = false
var action_gap = 200
onready var gobj = load("res://2.tscn")
#onready var ground = $ground
var config = null
var current_size = 0

func _ready():
	collision_layer = 513
	current_size = OS.get_window_size()
	#if config == null:
	#	config = preload("res://scripts/configs/levels_config.gd").new()
	#gobj.connect("gist_direction", self, "gist_direction")

func set_parent(p):
	parent = p

#func get_level_ground(lvl):
	#if config == null:
	#	config = preload("res://scripts/configs/levels_config.gd").new()
	#var ground = config.get_ground(lvl)
	#print("load %s" % ground)
	#gobj = load(ground)

func set_player(p):
	player = p
	player.connect("move", self, "check_move")
	#get_level_ground(player.level)
	#player.connect("gist_direction", self, "gist_direction")

func check_move(p):
	#$pos.text = "%s" % position.x
	if gobj and (position.x - (p.position.x+current_size.x)) <= 0 and not next:
		print("load")
		var g = gobj.instance()
		g.position.x =  position.x + (size.x)
		g.parent = parent
		g.set_player(player)
		parent.add_child(g)
		#g.ground.scale = g.ground.scale * core.scale_factor
		next = true
	#$actions_cont.rect_global_position.y = p.sensor.global_position.y + 20
	if (p.position.x - position.x) > (size.x+1500):
		print("unload")
		queue_free()
