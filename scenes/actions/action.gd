extends Sprite

var data = {
	"key": "",
	"action": "",
	"battle_action": "",
	"enemy_action": ""
}
var action = null
var bmode = false
var do_stop = false

func disable_bmode_act(is_battle):
	if bmode and not is_battle:
		get_node("action_area/collision").disabled = true
	else:
		get_node("action_area/collision").disabled = false
	if not bmode and is_battle:
		get_node("action_area/collision").disabled = true
	else:
		get_node("action_area/collision").disabled = false

func stop(battle_mode=false):
	if battle_mode and bmode:
		do_stop = true
	elif not battle_mode and not bmode:
		do_stop = true

func start():
	hide()
	#global_position.y = core.actions_offset
	if bmode:
		global_position.y = 35
	else:
		global_position.y = 20
	$ready.hide()

func ready():
	$ready.show()

func go():
	#$Spine.connect("animation_complete", self, "drop")
	$go.play("%s_action" % data.key)

func _on_go_animation_finished(anim_name):
	pass # Replace with function body.
	queue_free()

func run_hide():
	if not bmode:
		#$action_area/collision.disabled = core.battle_mode
		show()

func set_actions(val):
	data.key = val.key
	data.battle_action = val.battle_action
	data.enemy_action = val.enemy_action

func clear_all():
	queue_free()

func _move(d):
	#return
	#if visible:
	if bmode and not do_stop:
		position.x += d

func clear_battle():
	if bmode:
		queue_free()

func switch_action(for_battle):
	if $action_area/collision == null:
		return
	if bmode and for_battle:
		$action_area/collision.disabled = false
	elif bmode and not for_battle:
		$action_area/collision.disabled = true
	elif not bmode and for_battle:
		$action_area/collision.disabled = true
	elif not bmode and not for_battle:
		$action_area/collision.disabled = false

func disable_collision():
	$action_area/collision.disabled = true


func _on_action_visibility_changed():
	$action_area/collision.disabled = not visible
